const prompts = require('prompts');
const yargs = require('yargs').argv;
var yaml = require('write-yaml');
const fs = require('fs');

let x = 0;
let y = 0;

let operator2 = 0;
let methods = ["+", "-", "*", "/"];
let countingMethod = "";

let userAnswer = 0;
let answerArray = [];
let correctArray = [];

let response = 0;
let points = 0;

(async () => {
	if (yargs.o == "a") {
		operator2 = 0;
		countingMethod = "Addition";
	}
	else if (yargs.o == "s") {
		operator2 = 1;
		countingMethod = "Subtraction";
	}
	else if (yargs.o == "m") {
		operator2 = 2;
		countingMethod = "Multiplication";
	}
	else if (yargs.o == "d") {
		operator2 = 3;
		countingMethod = "Division";
	}
	console.log(countingMethod);
	console.log("Questions: " + yargs.q);
	console.log("Difficulty: 1 - " + yargs.d);
	console.log("User: " + yargs.user);

	for (var i = 0; i < yargs.q; i++) {
		x = Math.floor(Math.random() * yargs.d + 1);
		y = Math.floor(Math.random() * yargs.d + 1);

		await askQuestion(x,y,operator2);

		if (yargs.o == "a") {
			correctArray.push(x+y);
		}
		else if (yargs.o == "s") {
			correctArray.push(x-y);
		}
		else if (yargs.o == "m") {
			correctArray.push(x*y);
		}
		else if (yargs.o == "d") {
			correctArray.push(x/y);
		}
		answerArray.push(userAnswer);
	}

	console.log("Your answers: " + answerArray);
	console.log("Corrent answers: " + correctArray);

	for (var i = 0; i < yargs.q; i++) {
		if (answerArray[i] === correctArray[i]){
			points++;
		}		
	}

	let final = "Final: " + points + "/" + yargs.q;
	console.log(final);
	
	var fileData = {countingMethod, answerArray, correctArray, final};
	let fileLocation = "./data/" + yargs.user +"/";
	var readMe = {Instruction: "In this folder you will find the results for " + yargs.user};

	yaml(fileLocation + "README - " + " " + yargs.user + ".yml",readMe, function(err) {
		if(err) console.log(err);
		});
	
	fs.readdir(fileLocation, function(err, files){
  		yaml(fileLocation + yargs.user + (files.length) + ".yml",fileData, function(err) {
		if(err) console.log(err);
		});
		console.log("This is try nr " + (files.length)); 
	});

})();

async function askQuestion(tal1,tal2,metod){

	const response = await prompts({
	    type: "number",
	    name: "value",
	    message: "Vad blir " + tal1 + methods[metod] + tal2,

	});
	userAnswer=response.value;
}



